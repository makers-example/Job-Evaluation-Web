﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NexigoApi.Models
{
    public class RequestModel
    {
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public string Employee { get; set; }
        public string EvaluationBy { get; set; }
        public string Evaluation { get; set; }
        public string ProcessId { get; set; }
        public string Comment { get; set; }
        public string Action { get; set; }
        public string ActionBy { get; set; }
    }
    public class LastActivityModel
    {
        public string ActivityName { get; set; }
        public string Participant { get; set; }
    }
    public class CommentHistoryModel
    {
        public string Date { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public string Action { get; set; }
    }
}