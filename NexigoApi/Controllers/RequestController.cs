﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using NexigoApi.Models;
using NexigoApi.NextflowService;

namespace NexigoApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RequestController : ApiController
    {
        [HttpPost]
        public IHttpActionResult CreateJobEvaluation([FromBody]RequestModel data)
        {
            var nf = new FlowQuestWorkflowServiceClient();

            using (var dc = new MakersApplicationDataContext())
            {
                var maker = new Maker();
                maker.CreatedBy = data.CreatedBy;
                maker.CreatedDate = DateTime.Now;
                maker.Employee = data.Employee;
                maker.Evaluation = "";
                maker.EvaluationBy = data.EvaluationBy;
                maker.Status = "Draft";
                maker.SubmittedDate = DateTime.Now;

                TaskItemResult taskItemResult;
                var errorMessage = string.Empty;

                nf.CreateProcessInstance(8, "EvaluationMakers0", data.CreatedBy, out taskItemResult, out errorMessage);
                maker.ProcessId = taskItemResult.ProcessId;

                dc.Makers.InsertOnSubmit(maker);
                dc.SubmitChanges();
                nf.ExecActionInCurrentActv(8, taskItemResult.ProcessId, data.Action, maker.CreatedBy, null, data.Comment, "", false, out errorMessage);
            }
            return Ok("Success");
        }

        [HttpPost]
        public IHttpActionResult CreateJobEvaluationsa([FromBody]RequestModel data)
        {

            using (var dc = new MakersApplicationDataContext())
            {
                var maker = new Maker();
                maker.CreatedBy = data.CreatedBy;
                maker.CreatedDate = DateTime.Now;
                maker.Employee = data.Employee;
                maker.Evaluation = "";
                maker.EvaluationBy = data.EvaluationBy;
                maker.ProcessId = 1;
                maker.Status = "Draft";
                maker.SubmittedDate = DateTime.Now;


                dc.Makers.InsertOnSubmit(maker);
                dc.SubmitChanges();
            }
            return Ok("Success");
        }

        [HttpPost]
        public IHttpActionResult ApproveJobEvaluation([FromBody]RequestModel data)
        {
            using (var dc = new MakersApplicationDataContext())
            {
                var maker = dc.Makers.Where(o => o.ProcessId == int.Parse(data.ProcessId)).SingleOrDefault();
                if (maker != null)
                {
                    maker.Evaluation = data.Evaluation;
                    dc.SubmitChanges();
                }
                var errorMessage = string.Empty;
                var nf = new FlowQuestWorkflowServiceClient();
                nf.ExecActionInCurrentActv(8, int.Parse(data.ProcessId), data.Action, data.ActionBy, null, data.Comment, "", false, out errorMessage);


            }
            return Ok("Success");
        }

        [HttpPost]
        public IHttpActionResult GetCommentHistory([FromBody]string processId)
        {
            var nf = new FlowQuestWorkflowServiceClient();
            CommentHistoryResult[] commentHistoryResult;
            var errorMessage = string.Empty;
            nf.GetCommentHistory(8, int.Parse(processId), 1, 100, out commentHistoryResult, out errorMessage);
            var commentResult = new List<CommentHistoryModel>();


            foreach (var comment in commentHistoryResult)
            {
                if (comment.CompletedDate != null)
                {
                    commentResult.Add(new CommentHistoryModel
                    {
                        Date = comment.StartDate.Value.ToString(),
                        Action = comment.Response,
                        Comment = comment.Comment,
                        Name = comment.UserName
                    });
                }
            }
            return Ok(commentResult);
        }

        [HttpPost]
        public IHttpActionResult GetCurrentActivityUser([FromBody]string processId)
        {
            var nf = new FlowQuestWorkflowServiceClient();
            LastActivity[] lastActivity;
            var errorMessage = string.Empty;

            nf.GetLastActivityWithParticipant(8, processId, out lastActivity, out errorMessage);
            var lastAct = new LastActivityModel();
            lastAct.ActivityName = lastActivity[0].ActivityName;
            lastAct.Participant = lastActivity[0].Participant;

            //lastActivity[0].ActivityName =
            return Ok(lastAct);


        }


    }


}
