﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using NexigoApi.Models;
using System.Threading.Tasks;
using NexigoApi.NextflowService;

namespace NexigoApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HomeController : ApiController
    {
        public List<RequestModel> PendingReview([FromBody]string pinno)
        {
            var res = new List<RequestModel>();
            if (string.IsNullOrEmpty(pinno))
            {
                return res;
            }

            var cf = new FlowQuestWorkflowServiceClient();
            string errorMessage;

            List<TaskList> AllTaskItem = new List<TaskList>();
            TaskList[] listItemResult = { };
            var tenantId = 8;
            var tasklistEnd = false;
            var pageFlow = 1;
            var pageSizeFLow = 100;
            var WorkflowName = "EvaluationMakers0";
            string whereQuery = "where userid = '" + pinno + "' and ProcessName = '" + WorkflowName+"'";
            while (!tasklistEnd)
            {
                if (!cf.GetTaskListWithOrder(tenantId, whereQuery, pageFlow, pageSizeFLow, "", "", out listItemResult, out errorMessage))
                    throw new ApplicationException(errorMessage);

                AllTaskItem.AddRange(listItemResult.ToList());
                pageFlow++;
                if (listItemResult.Length == 0)
                {
                    tasklistEnd = true;
                }
            }

            var pids = AllTaskItem.Select(o => o.ProcessId);

            using (var dc = new MakersApplicationDataContext())
            {
                var result = (from o in dc.Makers
                             where pids.Contains((int)o.ProcessId)
                             select new RequestModel {
                                 ProcessId = o.ProcessId.ToString(),
                                 CreatedBy = o.CreatedBy,
                                 CreatedByName = o.CreatedBy,
                                 Employee = o.Employee,
                                 EvaluationBy= o.EvaluationBy
                             }).ToList();

                return result;
            }

        }

        [HttpPost]
        public List<RequestModel> MyDocument([FromBody]string pinno)
        {
            var res = new List<RequestModel>();
            if (string.IsNullOrEmpty(pinno))
            {
                return res;
            }

           
            using (var dc = new MakersApplicationDataContext())
            {
                var result = (from o in dc.Makers
                              where o.CreatedBy == pinno
                              select new RequestModel
                              {
                                  ProcessId = o.ProcessId.ToString(),
                                  CreatedBy = o.CreatedBy,
                                  CreatedByName = o.CreatedBy,
                                  Employee = o.Employee,
                                  EvaluationBy = o.EvaluationBy
                              }).ToList();
                return result;
            }

        }
    }
}
