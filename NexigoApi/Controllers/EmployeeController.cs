﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using NexigoApi.Models;

namespace NexigoApi.Controllers
{

    public class SelectResult
    {
        public string text { get; set; }
        public string value { get; set; }
    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EmployeeController : ApiController
    {
        public IHttpActionResult EmployeeList()
        {
            var result = new List<SelectResult>();
            using (var dc = new MakersApplicationDataContext())
            {
                var users = dc.Users.Where(o => o.ID != "2016022").ToList();

                foreach (var user in users)
                {
                    result.Add(new SelectResult()
                    {
                        text = user.Name,
                        value = user.ID
                    });
                }
            }
            return Ok(result);
        }

        [HttpPost]
        public IHttpActionResult GetEmployeeName([FromBody]string pinno)
        {
            var result = string.Empty;
            using (var dc = new MakersApplicationDataContext())
            {
                var user = dc.Users.Where(o => o.ID == pinno).SingleOrDefault();
                if (user != null)
                    result = user.Name;
                return Ok(result);
            }
        }
    }


}
