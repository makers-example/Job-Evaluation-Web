﻿nexigo.widget({
    text: 'Home',
    toolbars: [
        //{ text: 'login', action: 'login', icon: 'fa-login' }
    ],
    views: [
        {
            name: 'panel1',
            text: 'Pending Review',
            type: 'panel',
            fields: [
                {
                    type: 'grid',
                    text: 'Grid',
                    name: 'gridPendingReview',
                    onDblClick: 'doubleClick',
                    options: {
                        selectable: 'single'
                    },
                    fields: [
                      {
                          name: 'ProcessId',
                          text: 'Process Id',
                          type: 'text',
                      },
                      {
                          name: 'CreatedBy',
                          text: 'Created By',
                          type: 'text',
                      }
                    ],
                    data: []
                },
            ]
        },
        {
            name: 'panel2',
            text: 'My Document',
            type: 'panel',
            fields: [
                 {
                     type: 'grid',
                     text: 'Grid',
                     name: 'gridMyDocument',
                     onDblClick: 'doubleClick',
                     options: {
                         selectable: 'single'
                     },
                     fields: [
                       {
                           name: 'ProcessId',
                           text: 'Process Id',
                           type: 'text',
                       },
                       {
                           name: 'CreatedBy',
                           text: 'Created By',
                           type: 'text',
                       }
                     ],
                     data: []
                 },
            ]
        }
    ],
    functions: {
        init: function (xg, callback) {

            xg.grid.refresh('panel2', { params: { pinno: '2015001' } });
            window.loadGrid = 0;
            xg.ajax({
                url: 'http://localhost:31603/api/home/MyDocument',
                data: window.loginId,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    xg.each(res, function (item, index) {
                        xg.grid.addRow('gridMyDocument', item);
                    });
                    window.loadGrid = window.loadGrid + 1;
                    if (window.loadGrid == 2)
                    { xg.loading.hide(); }
                },
                complete: function () {
                    console.log("complete");
                    //xg.loading.hide();
                }
            });
            //gridPendingReview
            xg.ajax({
                url: 'http://localhost:31603/api/home/PendingReview',
                data: window.loginId,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    xg.each(res, function (item, index) {
                        xg.grid.addRow('gridPendingReview', item);
                    });
                    window.loadGrid = window.loadGrid + 1;
                    if (window.loadGrid == 2)
                    { xg.loading.hide(); }
                },
                complete: function () {
                    console.log("complete");
                    //xg.loading.hide();
                }
            });
            callback();
        },
        doubleClick: function (row) {
            console.log(row);
            xg.navigate('makers/JobEvaluation?processId=' + row.ProcessId);

        },
        login: function () {
            var data = xg.serialize('panel1');
            window.loginId = data.loginId;
        }
    }

});