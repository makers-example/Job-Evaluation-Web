﻿nexigo.widget({
    text: 'Job Evaluation',
    toolbars: [
        {
            text: 'Submit',
            icon: 'fa-save',
            action: 'Submit',
            name: 'Submit'
        },
        {
            text: 'Cancel',
            icon: 'fa-times',
            action: 'cancel',
            name: 'Cancel'
        },
        {
            text: 'Approve',
            icon: 'fa-approve',
            action: 'Approve',
            name: 'Approve',
            hide:true
        }
    ],
    views: [
        {
            name: 'panel1',
            type: 'panel',
            fields: [
                { name: 'CreatedBy', text: 'Created By', cols: 5, readonly: true },
                { name: 'CreatedByName', text: 'Name', cols: 5, readonly: true },
                { name: 'Employee', text: 'Employee', type: 'select', data: 'http://localhost:31603/api/employee/EmployeeList', cols: 5 },
                { name: 'EvaluationBy', text: 'Evaluation By', type: 'select', data: 'http://localhost:31603/api/employee/EmployeeList', cols: 5 },
                { name: 'Evaluation', text: 'Evaluation', type: 'textarea', cols: 5, readonly: true },

            ]
        },
        {
            name: 'panel2',
            type: 'panel',
            fields: [
                {
                    name: 'Comment',
                    type: 'textarea',
                    text: 'Comment'
                },
                {
                    name: 'commentGrid',
                    type: 'grid',
                    data: [],
                    fields: [
                        { name: 'Date', text: 'Date' },
                        { name: 'Name', text: 'Name' },
                        { name: 'Comment', text: 'Comment' }
                    ]
                }
            ]
        }
    ],
    functions: {
        Submit: function () {
            xg.loading.show();
            var data = xg.serialize('panel1');
            var dataComment = xg.serialize('panel2');
            data.Action = "Submit";
            data.Comment = dataComment.Comment;

            xg.ajax({
                url: 'http://localhost:31603/api/request/CreateJobEvaluation',
                data: JSON.stringify(data),
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    console.log(res);
                },
                complete: function () {
                    console.log("complete");
                    xg.notify({ type: 'success', text: 'Submit success' });
                    xg.loading.hide();
                    xg.navigate('makers/home');
                }
            });
        },

        Approve: function () {
            xg.loading.show();
            var data = xg.serialize('panel1');
            var dataComment = xg.serialize('panel2');
            data.Action = "Approve";
            data.Comment = dataComment.Comment;
            data.ProcessId = xg.params.data['processId'].toString();
            data.ActionBy = window.loginId;
            xg.ajax({
                url: 'http://localhost:31603/api/request/ApproveJobEvaluation',
                data: JSON.stringify(data),
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    console.log(res);
                },
                complete: function () {
                    console.log("complete");
                    xg.notify({ type: 'success', text: 'Approve success' });
                    xg.loading.hide();
                    xg.navigate('makers/home');
                }
            });
        },

        init: function (xg, callback) {
            if (window.loginId == '' || window.loginId == null) {
                xg.navigate('makers/login');
            } else {
                xg.ajax({
                    url: 'http://localhost:31603/api/employee/GetEmployeeName',
                    data: window.loginId,
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    success: function (res) {
                        console.log(res);
                        xg.populate({ CreatedBy: window.loginId, CreatedByName: res });
                    },
                    complete: function () {
                        console.log("complete");
                        xg.loading.hide();
                    }
                });

                //GetCommentHistory
                if (xg.params.data['processId'] != '' || xg.params.data['processId'] != null) {
                    var processId = xg.params.data['processId'].toString();

                    //Get Comment History
                    xg.ajax({
                        url: 'http://localhost:31603/api/request/GetCommentHistory',
                        data: processId,
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        success: function (res) {
                            console.log(res);
                            xg.each(res, function (item, index) {
                                xg.grid.addRow('commentGrid', item);
                            });
                        },
                        complete: function () {
                            console.log("complete");
                            xg.loading.hide();
                        }
                    });

                    //Get Last Activity                   
                    xg.ajax({
                        url: 'http://localhost:31603/api/request/GetCurrentActivityUser',
                        data: processId,
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        success: function (res) {
                            switch (res.ActivityName) {
                                case "Review Approval":
                                    xg.readonly(false, ['Evaluation']);
                                    xg.readonly(true, ['Employee', 'EvaluationBy']);
                                    xg.toolbar.hide('Submit');
                                    xg.toolbar.show('Approve');
                            }

                            if (res.Participant != window.loginId) {
                                xg.navigate('makers/home');
                            }
                        },
                        complete: function () {
                            console.log("complete");
                            xg.loading.hide();
                        }
                    });
                }
            }


            xg.loading.hide();
            callback();
        },

    }
});