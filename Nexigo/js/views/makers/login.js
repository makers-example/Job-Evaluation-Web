﻿nexigo.widget({
    text: 'Login',
    toolbars: [
        { text: 'login', action: 'login', icon: 'fa-login' },
        { text: 'logout', action: 'logout', icon: 'fa-logout' }
    ],
    views: [
        {
            name: 'panel1',
            type: 'panel',
            fields: [
                { name: 'loginId', text: 'Login ID' }
            ]
        }
    ],
    functions: {
        init: function (xg, callback) {
            callback();
        },
        login: function () {
            var data = xg.serialize('panel1');
            window.loginId = data.loginId;
            xg.navigate('makers/home');
        },
        logout: function () {
            window.loginId = '';
        }
    }

});